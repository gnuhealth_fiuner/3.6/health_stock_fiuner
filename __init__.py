from trytond.pool import Pool
from . import health_stock

def register():
    Pool.register(
        health_stock.PatientAmbulatoryCare,
        health_stock.PatientAmbulatoryCareMedicament,
        health_stock.PatientVaccination,
        module='health_stock_fiuner', type_='model')
