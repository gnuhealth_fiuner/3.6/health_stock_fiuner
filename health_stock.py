from trytond.model import Workflow, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['PatientAmbulatoryCare','PatientAmbulatoryCareMedicament',
            'PatientVaccination']

class PatientAmbulatoryCare(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ambulatory_care'

    @classmethod
    def __setup__(cls):
        super(PatientAmbulatoryCare, cls).__setup__()
        if not cls.medicaments.context:
            cls.medicaments.context = {}
        cls.medicaments.context.update({
            'patient': Eval('patient'),
            'state': Eval('state')
            })
        cls._buttons.update({
            'done_create_vaccination': {
                'invisible': ~Eval('state').in_(['draft']),
            }})

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done_create_vaccination(cls, ambulatory_cares):
        pool = Pool()
        Vaccination = pool.get('gnuhealth.vaccination')
        cls.done(ambulatory_cares)
        for ambulatory in ambulatory_cares:
            medicament_vaccines = [{
                'name': ambulatory.patient.id,
                'vaccine': medicament_line.medicament.id,
                'date': ambulatory.session_start,
                'next_dose_date': medicament_line.next_dose,
                'dose': medicament_line.dose,
                'observations': medicament_line.short_comment,
                'admin_route': medicament_line.admin_route,
                'amount': medicament_line.amount,
                'admin_site': medicament_line.admin_site,
                'picture': medicament_line.picture,
                'origin_selection': 'ambulatory_care'
                } for medicament_line in ambulatory.medicaments if medicament_line.medicament.is_vaccine == True]
            if medicament_vaccines:
                vaccines = Vaccination.create(medicament_vaccines)
                Vaccination.sign(vaccines)


class PatientAmbulatoryCareMedicament(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ambulatory_care.medicament'

    next_dose = fields.DateTime('Next dose',
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    dose = fields.Integer('Dose',
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    admin_route = fields.Selection([
        (None, ''),
        ('im', 'Intramuscular'),
        ('sc', 'Subcutaneous'),
        ('id', 'Intradermal'),
        ('nas', 'Intranasal'),
        ('po', 'Oral'),
        ], 'Route', sort=False,
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    picture = fields.Binary('Label',
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    amount = fields.Float(
        'Amount',
        help='Amount of vaccine administered, in mL . The dose per mL \
            (eg, mcg, EL.U ..) can be found at the related medicament',
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    admin_site = fields.Selection([
        (None, ''),
        ('lvl', 'left vastus lateralis'),
        ('rvl', 'right vastus lateralis'),
        ('ld', 'left deltoid'),
        ('rd', 'right deltoid'),
        ('lalt', 'left anterolateral fat of thigh'),
        ('ralt', 'right anterolateral fat of thigh'),
        ('lpua', 'left posterolateral fat of upper arm'),
        ('rpua', 'right posterolateral fat of upper arm'),
        ('lfa', 'left fore arm'),
        ('rfa', 'right fore arm')],'Admin Site',
        states={
            'invisible':~Eval('is_vaccine'),
            'readonly': Eval('state')=='done'
            })
    is_vaccine = fields.Function(
        fields.Boolean('Is vaccine'),
        'on_change_with_is_vaccine')
    state = fields.Function(
        fields.Char('State'),
        'on_change_with_state')

    @fields.depends('medicament')
    def on_change_with_is_vaccine(self, name=None):
        if self.medicament and self.medicament.is_vaccine:
            return True
        return False

    @fields.depends('medicament')
    def on_change_with_state(self, name=None):
        state = Transaction().context.get('state')
        if state == 'done':
            return 'done'

    @fields.depends('dose')
    def on_change_medicament(self, name=None):
        pool = Pool()
        Vaccination = pool.get('gnuhealth.vaccination')
        AmbulatoryCare = pool.get('gnuhealth.patient.ambulatory_care')

        patient_id = Transaction().context.get('patient')
        vaccines = []
        if patient_id and self.medicament and self.medicament.is_vaccine:
            vaccines = Vaccination.search([
                ('name','=',patient_id),
                ('vaccine','=',self.medicament.id)])
        if vaccines:
            self.dose = max([x.dose for x in vaccines])+1
            self.admin_route = vaccines[-1].admin_route
            self.amount = vaccines[-1].amount
            self.admin_site = vaccines[-1].admin_site
        else:
            self.dose = 1

    @classmethod
    def __setup__(cls):
        super(PatientAmbulatoryCareMedicament,cls).__setup__()
        cls.medicament.states['readonly'] = Eval('state')=='done'
        cls.quantity.states['readonly'] = Eval('state')=='done'
        cls.lot.states['readonly'] = Eval('state')=='done'
        cls.short_comment.states['readonly'] = Eval('state')=='done'
        cls.product.states['readonly'] = Eval('state')=='done'

class PatientVaccination(metaclass=PoolMeta):
    __name__ = 'gnuhealth.vaccination'

    origin_selection = fields.Selection([
        (None,''),
        ('ambulatory_care','Ambulatory Care')
        ],'Origin', sort=False)
    ambulatory_care = fields.Many2One(
        'gnuhealth.patient.ambulatory_care','Ambulatory Care',
        states={'invisible': Eval('origin_selection')=='ambulatory_care'})
